<?php

/**
 * ORM for Nette applications
 * 2014 http://lukasfessl.cz
 */

namespace Bean\ORM;

use Nette;
use Bean\ORM\Create;


/**
 * Container contains settigs for static class Create
 * @author Lukas Fessl
 */
class BeanContainer
{
    /**
     * @var Nette\Database\Context
     */
    public $connection;

    /**
     * Name of namespace to entities
     * @var String
     */
    public $namespace;

    /**
     * Name of namebuilder, include namespace
     * @var String
     */
    public $nameBuilder;



    public function __construct(Nette\Database\Context $connection, $namespace = NULL, $nameBuilder = NULL)
    {
        $this->connection = $connection;

        $this->namespace = $namespace;

        $this->nameBuilder = $nameBuilder;
    }

}
