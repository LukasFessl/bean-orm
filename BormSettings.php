<?php

namespace Bean\ORM;

use Nette;

/**
 * Router factory.
 */
class BormSettings
{
    /**
     * @var Nette\Database\Context
     */
    private static $connection;

    /**
     * Name of namespace to entities
     * @var String
     */
    private static $namespace = 'Bean\\ORM\\';


    /**
     * Setter for settings connection, namespace and nameBuilder from BeanContainer
     * @param  BeanContainer
     * @return void
     */
    public static function set(BeanContainer $bean){
        if(isset($bean->connection))
        self::$connection = $bean->connection;
        if(isset($bean->namespace))
            self::$namespace = $bean->namespace;
        if(isset($bean->nameBuilder))
            NameBuilderMapper::$builder = $bean->$nameBuilder;
    }


    public static function getConnection()
    {
        return self::$connection;
    }

    public static function getNamespace()
    {
        return self::$namespace;
    }
}
