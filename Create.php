<?php

/**
 * ORM for Nette applications
 * 2014 http://lukasfessl.cz
 */

namespace Bean\ORM;

use Nette;
use Nette\Utils\Strings;


/**
 * Static class for entities creation
 * @author Lukas Fessl
 */
abstract class Create extends Nette\Object
{


    public static function __callStatic($name, $arguments)
    {
        $class = BormSettings::getNamespace().Strings::firstUpper($name);
        $class = new $class();
        $class->setConnection(BormSettings::getConnection());
        return $class;
    }


}
