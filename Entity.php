<?php

/**
 * ORM for Nette applications
 * 2014 http://lukasfessl.cz
 */

namespace Bean\ORM;

use Nette;
use Nette\Utils\Strings;



/**
 * Class providing extension for your Entities
 * @author Lukas Fessl
 */
abstract class Entity extends Nette\Object
{
	/**
	 * @var String
	 */
	protected $_table;

	/**
	 * @var String
	*/
	protected $_tableSufix;

	/**
	 * @var String
	 */
	protected $_tablePrefix;

    /**
     * Key of array for findAll
     * @var String
     */
    protected $_index;

	/**
	 * Enitity watch update event
	 * @var bool
	 */
	protected $_lastUpdatedTimeStamp = false;

	/**
	 * Name of own varibable to update time
	 * @var String
	 */
	protected $_lastUpdatedName;

	/**
	* Enitity watch save event
	* @var bool auto
	*/
	protected $_dateCreatedTimeStamp = false;

	/**
	* Name of own varibable to save time
	* @var String
	*/
	protected $_dateCreatedName;

	/**
	 * @var Context
	 */
	protected $_connection;


	public function __construct()
	{
		$nameBuilder = NameBuilderMapper::$builder;
		$this->_table = $nameBuilder::tableName(get_class($this));
		$this->mapping();
		$this->goMapping();
	}


	/**
	 * @var Nette\Database\Context
	 * @return void
	 */
	public function setConnection(Nette\Database\Context $connection)
	{
		$this->_connection = $connection;
	}


	/**
	 * Overwrite from entity and contain settings of entity
	 */
	protected function mapping()
	{
		$mapping = array(

		);
		return $mapping;
	}



	protected function goMapping()
	{
		$map = $this->mapping();

		if(isset($map['table'])) {
			if(isset($map['table']['name']))
				$this->_table = $map['table']['name'];
			if(isset($map['table']['prefix']))
				$this->_tablePrefix = $map['table']['prefix'];
			if(isset($map['table']['sufix']))
				$this->_tableSufix = $map['table']['sufix'];
		}
		$this->_table = $this->_tablePrefix.$this->_table.$this->_tableSufix;

		if (isset($map['lastUpdated'])) {
			if(isset($map['lastUpdated']['timeStamp']))
				$this->_lastUpdatedTimeStamp = $map['lastUpdated']['timeStamp'];
			if(isset($map['lastUpdated']['name']))
				$this->_lastUpdatedName = $map['lastUpdated']['name'];
		}
		if (isset($map['dateCreated'])) {
			if(isset($map['dateCreated']['timeStamp']))
				$this->_dateCreatedTimeStamp = $map['dateCreated']['timeStamp'];
			if(isset($map['dateCreated']['name']))
				$this->_dateCreatedName = $map['dateCreated']['name'];
		}
        if (isset($map['index'])) {
            $this->_index = $map['index'];
        }

	}


	/**
	 * Get this entity by id
	 */
	public function get($id = NULL)
	{
		if ($id) {
			$this->bind($this->table($this->_table)->where('id', $id)->select('*')->fetch());
			return $this;
		} else if($id == NULL) {
			return $this;
		} else
			throw new \Exception("Parameter id must be number");
	}


	/**
	 * Get last insert
	 */
	public function getLast($order = 'id')
	{
		$this->bind($this->query("SELECT * FROM $this->_table ORDER BY $order DESC limit 0,1")->fetch());
		return $this;
	}



	public function setIndex($index) {
        $this->_index = $index;
        return $this;
    }


	/**
	 * Definitions of functions liek get, set, findBy, findAll, findAllby, count and countBy
	 * @var query - name of function
	 * @var arguments - arguments of function
	 */
	public function __call($query, $arguments)
	{
		$sqlBuilder = new SqlBuilder();

		switch ($query) {
			case Strings::startsWith($query, 'findBy'):
				$sql = $sqlBuilder->build($query, $arguments, $this->_table, 'findBy');
				$data = $this->query($sql)->fetch();
				return $this->buildObject($data);
				break;
			// case Strings::startsWith($name, 'get'):
			// 	$get = $sqlB->get($name);
			// 	return $this->$get['varName'];
			// 	break;
			//
			// case Strings::startsWith($name, 'set'):
			// 	$set = $sqlB->set($name);
			// 	$this->$set['varName'] = $arguments[0];
			// 	break;
			//
			case Strings::startsWith($query, 'findAllBy'):
				$sql = $sqlBuilder->build($query, $arguments, $this->_table, 'findAllBy');
				$data = $this->query($sql)->fetchAll();
				return $this->buildObjects($data);
				break;
			case Strings::startsWith($query, 'findAll'):
				$sql = $sqlBuilder->build($query, $arguments, $this->_table, 'findAll');
				$data = $this->query($sql)->fetchAll();
				return $this->buildObjects($data);
				break;

			case Strings::startsWith($query, 'countBy'):
				$sql = $sqlBuilder->build($query, $arguments, $this->_table, 'countBy');
				$data = $this->query($sql)->fetch();
				return $data[0];
				break;

			case Strings::startsWith($query, 'count'):
				$sql = $sqlBuilder->build($query, $arguments, $this->_table, 'count');
				$data = $this->query($sql)->fetch();
				return $data[0];
				break;

			default:
				throw new \Exception("Call to undefined method $name(".implode(',',$arguments).")");
				break;
		}
	}


	/**
	 * Buid object from array of data
 	 * @var Nette\Database\Row
	 * @return Object
	 */
	private function buildObject($data)
	{
		$name = get_class($this);
		$class = 'name';
		$instance = new $$class();
		$instance->setConnection($this->_connection);
		$instance->bind($data);
		return $instance;
	}


	/**
	 * Buid objects from array of data
	 * @var Nette\Database\Row\
	 * @return array<Object>
	 */
	private function buildObjects($data)
	{
		$instances = array();
		foreach ($data as $key => $values) {
			$instances[$this->_index ? $values[$this->_index] : $key] = $this->buildObject($values);
		}
		return $instances;
	}


	/**
	 * Delete self without parametr
	 * Delete by id another
	 * @return void
	 */
	public function delete($id = NULL)
	{
		if ($id) {
			return $this->_connection->table($this->_table)->where('id', $id)->delete();
		}
		else if ($this->id) {
			return $this->_connection->table($this->_table)->where('id', $this->id)->delete();
		}
	}


	/**
	 * Save or update entity
	 * @return saved entity
	 */
	public function save()
	{
		$this->_dateBuild();

		$this->beforeSave();

		$data = array();
		foreach (get_object_vars($this) as $key => $value) {

			if(!Strings::startsWith($key, '_') && !Strings::startsWith($key, 'n_') && get_class($this) != $key && $key != 'id')
			{
				$var = "key";
				$nameBuilder = NameBuilderMapper::$builder;
				$data[$nameBuilder::variableToAttribut($$var)] = $value;
			}
		}

		if ($this->id) {
			$res = $this->_connection->table($this->_table)->where('id', $this->id)->update($data);
			return $this;
		} else {
			$res = $this->_connection->table($this->_table)->insert($data);
			$this->id = $res['id'];
			return $this;
		}
	}


	/**
	 * Set date if allow
	 */
	private function _dateBuild()
	{
		$lun = $this->_lastUpdatedName;
		$dcn = $this->_dateCreatedName;

		if ($this->_lastUpdatedTimeStamp == true) {
			if(!empty($lun))
				$this->$lun = Date('Y-m-d H:i:s');
			else
				$this->lastUpdated = Date('Y-m-d H:i:s');
		}

		if($this->_dateCreatedTimeStamp == true && empty($this->id)) {
			if(!empty($dcn))
				$this->$dcn = Date('Y-m-d H:i:s');
			else
				$this->dateCreated = Date('Y-m-d H:i:s');
		}
	}



	/**
	 * Bind data from array to this object
	 */
	public function bind($params)
	{
		if ($params)
		{
			// $map = $this->mapping();
			foreach ($params as $key => $value)
			{
				$nameBuilder = NameBuilderMapper::$builder;
				$keyT = $nameBuilder::attributToVariable($key) ;
				$var = "keyT";
				$this->$$var = $value;
			}
			$this->afterBind();
		}
	}



	/**
	 * Function called after bind action

	 */
	protected function afterBind()
	{

	}

	/**
	 * Function called before saving data to db
	 * Overwrite from entity
	 */
	protected function beforeSave()
	{

	}






	public function query($q)
	{
		return $this->_connection->query($q);
	}


	public function table($table)
	{
		return $this->_connection->table($table);
	}


	public function getTableName()
	{
		return $this->_table;
	}


	public function __toString()
	{
		return $this->id ? $this->id : 'Not saved';
	}

}
