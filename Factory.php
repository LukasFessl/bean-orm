<?php
/**
* ORM for Nette applications
* 2014 http://lukasfessl.cz
**/

namespace Bean\ORM;


use Nette;
use Nette\Utils\Strings;

/**
* @author Lukas Fessl
*
* Factory creates new objects
*
* @property-read Context $_connection
* @roperty-read string $namespace
**/
class Factory extends Nette\Object
{
	/** @var Context of db connection**/
	private $connection;

	/** @var String of namespace**/
	private $_namespace;



	public function __construct(Nette\Database\Context $connection, $namespace = "Bean\ORM\\", $nameBuilder = "Bean\ORM\NameBuilder")
	{
		$this->connection = $connection;
		$this->_namespace = $namespace;
		NameBuilderMapper::$builder = $nameBuilder;
	}


	/**
	* @property-read string $name function name
	* @roperty-read array[] $arguments parametrs of function
	**/
	public function __call($name, $arguments)
	{
		$id = NULL;

		if (count($arguments) == 1)
		{
			 is_numeric($arguments[0]) || is_null($arguments[0]) ? $id = $arguments[0] : $this->_namespace = $arguments[0];
		}
		else if (count($arguments) == 2)
		{
			if(is_numeric($arguments[0]) || is_null($arguments[0]) )
			{
				$id = $arguments[0];
				$this->_namespace = $arguments[1];
			}
			else
			{
				$id = $arguments[1];
				$this->_namespace = $arguments[0];
			}
		}

		if (Strings::match($name, '~^create[A-Z]{1}~'))
		{
			$class = $this->_namespace.Strings::replace($name, '~^create~', '');
			$class = new $class();
			$class->setConnection($this->connection);
			if(!is_null($id))
				$class->get($id);
			return $class;
		}
		else
			throw new Nette\MemberAccessException("Call to undefined method $this->_namespace"."Factory::$name().");
	}


	/**
	* Setter
	* @roperty-read string $namespace
	**/
	public function setNamespace($namespace)
	{
		$this->_namespace = $namespace;
	}
}
