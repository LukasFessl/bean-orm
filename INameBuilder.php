<?php
/**
* ORM for Nette applications
* 2014 http://lukasfessl.cz
**/

namespace Bean\ORM;

/**
* interface for NameBuilder
**/
interface INameBuilder
{
	/**
	* Change string - mapping var name to db column name
	* @return string
	**/
	public static function variableToAttribut($str);



	/**
	* Change string - mapping column name to object var name
	* @return string
	**/
	public static function attributToVariable($str);


	/**
	* Build table name from objectName
	* @return string
	**/
	public static function tableName($str);

}