<?php
/**
* ORM for Nette applications
* 2014 http://lukasfessl.cz
**/

namespace Bean\ORM;

use Nette;
use Nette\Utils\Strings;
use Exception;


/**
* @author Lukas Fessl
*
* Name builder for class name and class variable to db name and db column name
**/
abstract class NameBuilder extends Nette\Object implements INameBuilder
{


	/**
	* Change string (from johnDoe to john_doe) - mapping var name to db column name
	* @return string
	**/
	public static function variableToAttribut($str)
	{
		return Strings::replace($str,'~([a-z]{1})([A-Z]{1})~', function($m){
			return $m[1]."_".Strings::lower($m[2]);
		});		
	}



	/**
	* Change string (from john_doe to johnDoe) - mapping column name to object var name
	* @return string
	**/
	public static function attributToVariable($str)
	{
		return Strings::replace($str,'~(\_{1})([a-z]{1})~', function($m){
			return Strings::upper($m[2]);
		});
	}



	/**
	* Build table name from objectName
	* @return string
	**/
	public static function tableName($str)
	{
		$name = Strings::replace($str, '/\\\\*[a-zA-Z]+\\\\/', '');
		$name = Strings::replace($name,'~([a-z]{1})([A-Z]{1})~', function($m){
			return $m[1]."_".Strings::lower($m[2]);
		});
		$nameLower = Strings::lower($name);

		return $nameLower;	
	} 



}

