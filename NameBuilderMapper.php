<?php
/**
* ORM for Nette applications
* 2014 http://lukasfessl.cz
**/

namespace Bean\ORM;


/**
* Here is stored name of NameBuilder + namespace
**/
abstract class NameBuilderMapper
{

	public static $builder = 'Bean\ORM\NameBuilder';

}
