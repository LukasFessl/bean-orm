**Small ORM for Nette aplication.**
Version 0.3.0

Copy Bean ORM into Others in vendor, extend you object about Entity
You have to create factory in neon serivce for creating objects - for example:

factory: class:Bean\ORM\Factory(@database)

@database = Nette\database\context

second parametr is optional, define namespace of you Entities. If your entities have namespace Bean\ORM you can not care about it.

If your entities have namespace for example Model then:
factory: class:Bean\ORM\Factory(@database, 'Model\')


You can set namespace in you presenter. You have to call function 'factory'->setNamespace('Model\'). It set namepsace for factory in presenter.

ORM have:

1.mapper for set table name, prefix and sufix. Auto generation date time for create and update.

2.Easy manipulation with objects. ORM have save, delete function.

3.You can call function get with parametr Id. you get Object from db by Id

4.You can build sql easy. Keywords -> findBy, findAll, findAllBy, count, countBy, bind, get and set

5.trigered function afterBind and beforeSave in object

6.auto papping Object name for ex. User to db with name user

7.auto mappgin object attribut for ex. firstName to db with column name first_name


Examples:


```
#!php

<?php

namespace Model;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class User extends Entity
{
	public $id = NULL;
	public $email;
	public $password;
	public $enabled;
	public $role;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'table' => array('name'=>'user', 'prefix'=>'d_', 'sufix'=>'_d')
			'lastUpdated' => array('timeStamp' => true,),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}


	public function afterBind()
	{
		$this->password = \AdminModule\HashPassword::hash($this->password);
	}
}

```

```
#!php

class UserPresenter extends BasePresenter
{
	protected $factory;


	public function startup()
	{
		parent::startup();
                $this->factory = $this->context->factory;
	}

        public function renderList()
	{

	     $this->template->users = $this->factory->createUser()->findAllOrder('id');
             //$this->factory->createUser()->get(1) // get user with id 1
             //$this->factory->createUser()->findById(1) // get user with id 1
             //$this->factory->createUser()->findAll() // get array of users
             //$this->factory->createUser()->findAllByEnabld(1) // get array of users
             //$this->factory->createUser()->findAllByEnabldAndId(1, ['<>', 5]) // get array of users
             //$this->factory->createUser()->findAllByIdOrder(['>', 1], 'id DESC')
             //$this->factory->createUser()->findByEnabledLike(1,['email', %john%]) // get user with id 1
	}

	public function createComponentUserForm()
	{
		$user = $this->factory->createUser()->findById($this->getParameter('id'));
		return new UserForm($user);
	}
}


```




```
#!php


<?php
namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

class UserForm extends Control
{

	private $user;

	public function __construct($user = NULL)
	{
		$this->user = $user;
	}

	public function createComponentUserForm()
	{
		$form = new Form();
                ....

		return $form;
	}


	public function processUser($form)
	{

	     $val = $form->getValues();
	     $this->user->bind($val);    //for binding name of input have to same name as object parametrs. Or you can use set function (setId($val[id]), setEmail($val[email]), ...)
	     $this->user->save();
	     $this->flashMessage("Změny uloženy");
	}



	public function render()
	{
		$this->template->setFile(__DIR__.'/UserForm.latte');
		$this->template->render();
	}

}
?>
```


**Update 0.6.1**(26.8.2014)

```
#!php

$this->factory->createUser();				// create empty user

$this->factory->createUser(1);				// create user and try get user with id 1, no exist return empty user

$this->factory->createUser('Entities\');		// create empty user from namespace \Entities

$this->factory->createUser(1, 'Entities\');	// create user from namespace \Entities and try get user with id 1, no exist return empty user

$this->factory->createUser('Entities\', 1);	// create user from namespace \Entities and try get user with id 1, no exist return empty user
```



**Update 0.6.2**(27.8.2014),(update 28.8.2014)

Now you can define yout own nameBuilder. Name builder is used when attributs in object are mapped into db column. In cofig.neon:

```
#!php

factory:
		class: Bean\ORM\Factory
		arguments:
			connection: @database
			namespace: 'Model\'
			nameBuilder: 'Model\NB\TestBuilder'
```

Arguments nameBuilder - define name your new nameBuilder as TestBuilder.


```
#!php

namespace Model\NB;

abstract class TestBuilder implements INameBuilder
{

	public static function variableToAttribut($str)
	{...}

	public static function attributToVariable($str)
	{...}

	public static function tableName($str)
	{...}
}

```
