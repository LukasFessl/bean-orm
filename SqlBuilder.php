<?php

/**
 * ORM for Nette applications
 * 2014 http://lukasfessl.cz
 */

namespace Bean\ORM;

use Nette;
use Nette\Utils\Strings;
use Exception;


/**
 * SqlBuider buid from name of function sql query
 * @author Lukas Fessl
 */
class SqlBuilder extends Nette\Object
{
	/**
	 * Build sql String
	 * @var allQuery - Strings (findAllById)
	 * @var arguments - array
	 * @var tableName - String
	 * @var keyWord - Strings (findBy)
	 * @return String
	 */
	public function build($allQuery, $arguments, $tableName, $keyWord)
	{
		$query = $this->_removeKeyWord($allQuery, $keyWord);
		$query[2] = $query[1];
		$query[1] = $tableName;
		$query = $this->_splitQuery($query);
		$sql = $this->_buidSql($query, $arguments);
		// dump($query);
		// dump($sql);
		return $sql;
	}


	/**
	 * Remove keyWord from String
	 * @var query - Strings (findAllById)
	 * @var keyWord - array (findAllBy)
	 * @return String
	 */
	private function _removeKeyWord($query, $keyWord)
	{
		return array($keyWord, Strings::replace($query, '~^'.$keyWord.'~', ''));
	}


	/**
	 * Build sql
	 * @var query - array
	 * @var arguments - array
	 * @return String
	 */
	private function _buidSql($query, $arguments)
	{
		$nb = NameBuilderMapper::$builder;

		$keyWords = array('findBy' => 'SELECT * FROM '.$query[1].' WHERE ',
						  'findAllBy' => 'SELECT * FROM '.$query[1].' WHERE ',
						  'findAll' => 'SELECT * FROM '.$query[1],
						  'countBy' => 'SELECT COUNT(*) as count FROM '.$query[1].' WHERE ',
						  'count' => 'SELECT COUNT(*) as count FROM '.$query[1]);
		$sql = $keyWords[$query[0]];

		$argKey = 0;
		foreach ($query[2] as $item) {
			if ($item == "And") {
				$sql .= " && ";
			} else if ($item == "Or") {
				$sql .= " || ";
			} else {
				if (is_array($arguments[$argKey])) {
					if(is_null($arguments[$argKey][1])) {
						$arg = $arguments[$argKey][0]." NULL";
					} else {
						$arg = $arguments[$argKey][0]." '".$arguments[$argKey][1]."'";
					}
				} else {
					if(is_null($arguments[$argKey])) {
						$arg = "IS NULL";
					} else {
						$arg = "= '".$arguments[$argKey]."'";
					}
				}
				// $arg = is_array($arguments[$argKey]) ? ($arguments[$argKey][0]." '".$arguments[$argKey][1])."'" : "= '".$arguments[$argKey]."'";
				$sql .= $this->_toLowerAndBuildName($item)." ".$arg;

				$argKey++;
			}

		}

		foreach ($query[3] as $item) {
			if ($item == "Order")
				$sql .= " ORDER BY ".$arguments[$argKey]."";
			else if ($item == "Limit")
				$sql .= " LIMIT ".$arguments[$argKey];

			$argKey++;
		}
		// dump($sql);
		return $sql;
	}


	/**
 	 * Split function name into array
	 * @var query - String
	 * @return array
	 */
	private function _splitQuery($query)
	{
		$match = Strings::replace($query[2], '~(And|Or)([A-Z])~', function($m){
			return "_".$m[1]."_".$m[2];
		});

		$keyWords = array('Like', 'Limit', 'Order');
		$usedKeyWords = array();

		foreach($keyWords as $keyWord) {
			$command = Strings::match($match, '~'.$keyWord.'$~');
			if ($command) {
				$match = Strings::replace($match, '~'.$command[0].'$~', '');
				array_unshift($usedKeyWords, $keyWord);
			}
		}

		$match = Strings::split($match, '~_~');
		$countArguments = 0;
		for ($i = 0; $i < count($match); $i++) {
			if($match[$i] != "And" && $match[$i] != "Or") {
				$countArguments++;
			}
		}

		$match[0] != "" ? $query[2] = $match : $query[2] = array();
		$query[3] = $usedKeyWords;
		$query[4] = $countArguments + count($usedKeyWords);
		return $query;
	}


	/**
	 * Set first letter to lower and upper letter into _ and lower (FirstName => first_name)
	 * @var String
	 * @return String
	 */
	private function _toLowerAndBuildName($string)
	{
		$nameBuilder = NameBuilderMapper::$builder;
		return Strings::lower($nameBuilder::variableToAttribut($string));
	}

}
