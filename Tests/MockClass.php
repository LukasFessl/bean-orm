<?php
/**
* Mock class for tests
* 2014 http://lukasfessl.cz
* version 0.2
**/

namespace Bean\ORM\Test;

use Bean\ORM\Entity;


/**
* @author Lukas Fessl
*
* Class implements MockClass
**/
class MockClass extends Entity
{
	public $id;
	public $name;
	public $firstName;
	public $lastName;
	public $notes; 

	protected $step;



	public function __construct($step = 1)
	{
		$this->step = $step;
		$this->id = 1;
		$this->name = "user";
		$this->firstName = 'John';
		$this->lastName = 'McClane';
		$this->notes = 'poznamka';
		parent::__construct();

	}



	protected function mapping()
	{
		if($this->step == 2)
		{
			$mapping = array(
				'table' => array('name'=>'user', 'prefix'=>'d_', 'sufix'=>'_d')
			);
			return $mapping;
		}
	}


}