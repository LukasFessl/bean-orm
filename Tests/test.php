<?php
/**
* Test class for Bean ORM
* 2014 http://lukasfessl.cz
* version 0.2
**/

namespace Bean\ORM\Test;

use Bean\ORM;


/**
* @author Lukas Fessl
*
* Class implements tests
*
* @property-read show - if false see only false tests
**/
class Test
{

	/** @var MockClass**/
	private $mockClass;

	/** @var bool**/
	private $show;


	public function __construct($show = true)
	{
		$this->mockClass = new mockClass();
		$this->show = $show;
	}


	/**
	* function testing findAll
	**/
	public function testFindAll()
	{
		$sqlB = new ORM\SqlBuilder();
		$sql = $sqlB->findAll('findAll', NULL);
		$sql = $sqlB->build($sql, NULL, 'test', '');
		$this->assert($sql, "SELECT * FROM test ", 'findAll');
	}


	/**
	* function testing findAllBy
	**/
	public function testFindAllBy()
	{
		$sqlB = new ORM\SqlBuilder();
		$query = 'findAllByName';
		$sql = $sqlB->findAllBy($query, NULL);
		$sql = $sqlB->build($sql,  array('john'), 'test', 'WHERE ');
		$this->assert($sql, "SELECT * FROM test WHERE name = 'john'", $query);

		$sqlB = new ORM\SqlBuilder();
		$query = 'findAllByNote';
		$sql = $sqlB->findAllBy($query, NULL);
		$sql = $sqlB->build($sql,  array('text'), 'test', 'WHERE ');
		$this->assert($sql, "SELECT * FROM test WHERE note = 'text'", $query);

		$sqlB = new ORM\SqlBuilder();
		$query = 'findAllByNoteAndId';
		$sql = $sqlB->findAllBy($query, NULL);
		$sql = $sqlB->build($sql,  array('text', 1), 'test', 'WHERE ');
		$this->assert($sql, "SELECT * FROM test WHERE note = 'text' && id = '1'", $query);

		$sqlB = new ORM\SqlBuilder();
		$query = 'findAllByNoteOrNotId';
		$sql = $sqlB->findAllBy($query, NULL);
		$sql = $sqlB->build($sql,  array('text', 1), 'test', 'WHERE ');
		$this->assert($sql, "SELECT * FROM test WHERE note = 'text' ||  not id = '1'", $query);

		$sqlB = new ORM\SqlBuilder();
		$query = 'findAllByNoteOrFirstNameOrderLimit';
		$sql = $sqlB->findAllBy($query, NULL);
		$sql = $sqlB->build($sql,  array('text','john', 'id desc',[0,1]), 'test', 'WHERE ');
		$this->assert($sql, "SELECT * FROM test WHERE note = 'text' || first_name = 'john' order by id desc limit  0, 1 ", $query);
	}



	/**
	* function testing building table name
	**/
	public function testTableName()
	{
		$mockClass = new MockClass();
		$this->assert($mockClass->getTableName(), 'mock_class', 'tableName1');

		$mockClass = new MockClass(2);
		$this->assert($mockClass->getTableName(), 'd_user_d', 'tableName2');
	}



	/**
	* run all tests
	**/
	public function runAll()
	{
		$this->testFindAll();
		$this->testFindAllBy();
		$this->testTableName();
	}



	/**
	* presentattion results
	**/
	private function assert($result, $expect, $query = NULL)
	{
		$res = array('result' => $result, 'expect' => $expect);
		if($result == $expect)
			array_unshift($res, $query, true);
		else
			array_unshift($res, $query, false);


		if($this->show == true)
			dump($res);
		else if($res[1]==false)
			dump($res);	
	}


}